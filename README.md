# CHESS S2I Images

This repository contains the code to build images for use with the **s2i** 
tool.  It contains both build images (-s2i) and runtime images (-runtime).

Most of the ideas come from the [CentOS sclorg s2i-containers 
repositories](https://github.com/sclorg/).
