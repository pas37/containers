#!/bin/sh
#
# Script for building container images
#
set -ex

CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-pas37/containers}
OS="sl7"

if [ -z "$1" -o ! -d "$1" ]; then
    echo "Usage: build.sh <container_name>"
    exit 1
fi

IMAGE="${OS}-$1"
IMAGE_TAG="${IMAGE}:latest"
FULL_IMAGE_TAG="${CI_REGISTRY_IMAGE}/${IMAGE_TAG}"

[ "$(id -u)" != "0" ] && SUDO="sudo"

${SUDO} docker build --pull -t ${FULL_IMAGE_TAG} $1
